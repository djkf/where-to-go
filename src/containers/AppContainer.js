import { Container } from 'flux/utils';
import { Dispatcher } from 'flux';
import { filter } from 'lodash';

import AppStore from '../stores/AppStore';
import AppActions from '../actions/AppActions';
import App from '../components/App';

const appDispatcher = new Dispatcher();

const appStore = new AppStore({
	dispatcher: appDispatcher,
});
const appActions = AppActions({
	dispatcher: appDispatcher,
});

const stores = () => [ appStore ];

const mapState = () => {
	const { users, venues } = appStore.getState();

	return {
		users,
		venues: filter(venues, { bad: false }),
		badVenues: filter(venues, { bad: true }),
		toggleUser: appActions.toggleUser,
	};
};

export default Container.createFunctional(App, stores, mapState);
