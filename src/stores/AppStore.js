import { ReduceStore } from 'flux/utils';
import { uniqueId } from 'lodash';

import { SELECT_USER, UNSELECT_USER } from '../constants/ActionTypes';
import { filterVenueByFoodDislikes, filterVenueByDrinkLikes } from '../filters';
import users from '../fixtures/users.json';
import venues from '../fixtures/venues.json';

export default class AppStore extends ReduceStore {
	constructor({ dispatcher }) {
		super(dispatcher);
	}

	static createInitialUserList() {
		return users.map((user) => ({
			...user,
			going: false,
			wont_eat: user.wont_eat.map(item => item.toLowerCase()),
			drinks: user.drinks.map(item => item.toLowerCase()),
			uuid: uniqueId('user_'),
		}));
	}

	static createInitialVenueList() {
		return venues.map((venue) => ({
			...venue,
			food: venue.food.map(item => item.toLowerCase()),
			drinks: venue.drinks.map(item => item.toLowerCase()),
			reasons: [],
			bad: false
		}));
	}

	getInitialState() {
		return {
			users: AppStore.createInitialUserList(),
			venues: AppStore.createInitialVenueList(),
		};
	}

	reduce(state, { type, response }) {
		let venueList, userList;

		const initialVenueList = AppStore.createInitialVenueList();

		switch (type) {
			case SELECT_USER:
				userList = state.users.map((user) => {
					if (response.uuid === user.uuid) {
						user.going = true;
					}

					return user;
				});

			 venueList = filterVenueByDrinkLikes(filterVenueByFoodDislikes(initialVenueList, userList), userList);

				return {
					users: userList,
					venues: venueList,
				};

			case UNSELECT_USER:
				userList = state.users.map((user) => {
					if (response.uuid === user.uuid) {
						user.going = false;
					}

					return user;
				});

				venueList = filterVenueByDrinkLikes(filterVenueByFoodDislikes(initialVenueList, userList), userList);

				return { users: userList, venues: venueList };

			default:
				return state;
		}
	}
}
