import { filter, intersection, difference } from 'lodash';

export const filterVenueByFoodDislikes = (venues, users) => {
	return venues.map((venue) => {

		filter(users, { going: true }).forEach(({ name, wont_eat }) => {
			if (!difference(venue.food, wont_eat).length) {
				venue.bad = true;
				venue.reasons.push(`${ name } wont eat ${ wont_eat.join(', ') }`);
			};
		});

		return venue;
	});
}

export const filterVenueByDrinkLikes = (venues, users) => {
	return venues.map((venue) => {
		filter(users, { going: true }).forEach(({ name, drinks }) => {
			if (!intersection(venue.drinks, drinks).length) {
				venue.bad = true;
				venue.reasons.push(`${ name } wants ${ drinks.join(', ') }`);
			};
		});

		return venue;
	});
}
