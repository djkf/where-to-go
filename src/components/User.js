import React from 'react';

export default ({ user, onChange }) => {
	const _onChange = (e) => {
		onChange(user);
	};

	const selected = user.going ? 'primary' : 'info';

	return (
		<button className={ `btn btn-xs btn-${ selected }` } onClick={ _onChange } role="button"><span className="glyphicon glyphicon-user" aria-hidden="true"></span> { user.name }</button>
	);
}
