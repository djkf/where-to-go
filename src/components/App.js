import React, { Component } from 'react';
import { isEmpty } from 'lodash';

import User from './User';

class App extends Component {
	constructor() {
		super();
		this._onChange = this._onChange.bind(this);
	}

	render() {
		const { users, venues, badVenues } = this.props;

		return (
			<div>
				<nav className="navbar navbar navbar-default navbar-fixed-top">
					<div className="container">
						<div className="navbar-header">
							<a className="navbar-brand" href="#"><span className="glyphicon glyphicon-gift" aria-hidden="true"></span> Where to go?</a>
						</div>
					</div>
				</nav>
				<div className="container">
					<h2>Who is going?</h2>

					{ users.map((user, index) => <User user={ user } key={ index } onChange={ this._onChange } />) }

					<h4>Places to Go</h4>
					{ venues.map( ({ name }, index) => {
						return (
							<li key={ index } className="list-group-item list-group-item-success">
								<span className="glyphicon glyphicon-ok" aria-hidden="true"></span> <strong>{ name }</strong>
							</li>
						);
					}) }

					<h4>Places to Avoid</h4>
					{ badVenues.map( ({ name, reasons }, index) => {
						const type = reasons.length > 1 ? 'danger' : 'warning';

						return (
							<li key={ index } className={ `list-group-item list-group-item-${ type }` }>
								<span className="glyphicon glyphicon-remove" aria-hidden="true"></span> <strong>{ name }</strong> because { reasons.join(' and ') }
							</li>
						);
					}) }
				</div>
			</div>
		);
	}

	_onChange(user) {
		this.props.toggleUser(user);
	}
}

export default props => <App { ...props } />
