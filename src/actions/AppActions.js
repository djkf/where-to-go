import { SELECT_USER, UNSELECT_USER } from '../constants/ActionTypes';

export default ({ dispatcher }) => {
	return {
		toggleUser: (user) => {
			if (user.going) {
				dispatcher.dispatch({
					type: UNSELECT_USER,
					response: user
				});
			} else {
				dispatcher.dispatch({
					type: SELECT_USER,
					response: user
				});
			}
		}
	};
};
