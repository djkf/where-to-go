const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: './src/app.js',
	output: {
		path: path.join(process.cwd(), '.tmp'),
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: [ 'es2015', 'stage-2', 'react' ]
				}
			},
			{
				test: /\.css$/,
				loaders: [ 'style-loader', 'css-loader' ]
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			}
		]
	},
	externals: {
		'react/addons': true,
		'react/lib/ExecutionEnvironment': true,
		'react/lib/ReactContext': true
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.join(process.cwd(), './statics/index.html')
		})
	]
};